from .dnstwist import dnstwist


class ScanAlgorithm:

    @staticmethod
    def scan(domain):
        domains = dnstwist.generate_domains(['--registered', '--threads', '20', '--format', 'cli', domain])
        return domains
