
# https://youtu.be/16s3Pi1InPU
"""
Comparing images using ORB/SIFT feature detectors
and structural similarity index. 
@author: Sreenivas Bhattiprolu
"""

from skimage.metrics import structural_similarity
from skimage.transform import resize
import cv2


# Works well with images of different dimensions
def sift_sim(img1, img2):
    # SIFT is no longer available in cv2 so using ORB
    orb = cv2.SIFT_create()

    # detect keypoints and descriptors
    kp_a, desc_a = orb.detectAndCompute(img1, None)
    kp_b, desc_b = orb.detectAndCompute(img2, None)

    if len(kp_a) == 0 or len(kp_b) == 0:
        return 0

    # define the bruteforce matcher object
    bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)

    # perform matches.
    matches = bf.match(desc_a, desc_b)
    # Look for similar regions with distance < 50. Goes from 0 to 100 so pick a number between.
    similar_regions = [i for i in matches if i.distance < 50]
    if len(matches) == 0:
        return 0
    return len(similar_regions) / len(matches)


# Needs images to be same dimensions
def structural_sim(img1, img2):
    sim, diff = structural_similarity(img1, img2, full=True, multichannel=False)
    return sim


def get_similarity(img1, img2):
    if img1.shape[0] != img2.shape[0] or img1.shape[1] != img2.shape[1]:
        img2 = resize(img2, (img1.shape[0], img1.shape[1]),  anti_aliasing=True, preserve_range=True)
    sim_sift = sift_sim(img1, img2)
    ssim = structural_sim(img1, img2)
    result = (ssim + sim_sift) / 2.0
    return result
