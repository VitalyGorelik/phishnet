#!/bin/sh

sed -i '$ d' phishnet_web/.env.prod

NAME=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname | cut -d '@' -f1)

echo $NAME

echo DOMAIN_NAME=$NAME >> phishnet_web/.env.prod

sudo docker-compose up --build

