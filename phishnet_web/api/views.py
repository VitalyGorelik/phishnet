from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from reports.models import Reports, Suspects
from users.email_service import EmailUtil


@csrf_exempt
def reportapi(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        report_id = body['id']
        suspects = body['suspects']
        try:
            report = Reports.objects.filter(id=report_id)
            report.update(status='Done')
            #report.save()
        except Exception as e:
            print(f'issue in api/views fetching a report, printing stacktrace:\n{e}')

        for suspect in suspects:
            try:
                report_suspects = Suspects(report_id=report_id,
                                           url=suspect['domain'], ip=suspect['ip'],
                                           score=suspect['score'], type_of_match=suspect['type_of_match'])
                report_suspects.save()
            except Exception as e:
                print(f'issue in api/views creating a suspect, printing stacktrace:\n{e}')

        # send email for the domain completed
        current_report = Reports.objects.filter(id=report_id).first()
        email = current_report.author.email
        username = current_report.author.username
        domain = current_report.domain
        try:
            EmailUtil.send_message(email, username, domain)
        except Exception as e:
            print(f'issue in api/views sending an email, printing stacktrace:\n{e}')
        return HttpResponse(request, 200)
    return HttpResponse(request, 400)
