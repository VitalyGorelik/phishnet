from django.db import models
from django.utils.translation import gettext_lazy
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser, BaseUserManager

# Create your models here.


class CustomAccountManager(BaseUserManager):

    def create_user(self, email, username, password, risk_level, reports_interval, domain1, domain2='',
                    domain3='', domain4='', domain5='',  **other_fields):
        if not email:
            raise ValueError(gettext_lazy('You must provide an email address.'))
        if not username:
            raise ValueError(gettext_lazy('You must provide a username.'))
        if not domain1:
            raise ValueError(gettext_lazy('You must provide a domain.'))
        if not password:
            raise ValueError(gettext_lazy('You must provide a password.'))
        email = self.normalize_email(email)
        user = self.model(email=email, username=username, reports_interval=reports_interval, risk_level=risk_level, domain1=domain1,
                          domain2=domain2, domain3=domain3, domain4=domain4, domain5=domain5, **other_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, username, password, risk_level, reports_interval, domain1, domain2='', domain3='', domain4=''
                         , domain5='', **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)
        return self.create_user(email, username, password, risk_level, reports_interval, domain1, domain2, domain3,
                                domain4, domain5, **other_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField('email', unique=True)
    username = models.CharField(max_length=50, unique=True)
    domain1 = models.URLField(max_length=50)
    domain2 = models.URLField(max_length=50, blank=True, null=True)
    domain3 = models.URLField(max_length=50, blank=True, null=True)
    domain4 = models.URLField(max_length=50, blank=True, null=True)
    domain5 = models.URLField(max_length=50, blank=True, null=True)
    report_interval_choices = [
        ('1', 'Daily'),
        ('7', 'Weekly'),
        ('30', 'Monthly'),
    ]
    # Risk level: low: 0, med:1, high: 2

    reports_interval = models.CharField(
        max_length=12,
        choices=report_interval_choices,
        default=report_interval_choices[2]
    )
    risk_level_choices = [
        ('0', 'Low'),
        ('1', 'Medium'),
        ('2', 'High'),
    ]
    risk_level = models.CharField(
        max_length=12,
        choices=risk_level_choices,
        default=risk_level_choices[2]
    )
    has_started = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'domain1', 'domain2', 'domain3', 'domain4', 'domain5',
                       'risk_level', 'reports_interval']

    objects = CustomAccountManager()

    def __str__(self):
        return self.email
