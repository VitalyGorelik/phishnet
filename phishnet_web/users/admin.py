from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser
from .forms import CustomUserCreationForm, CustomUserChangeForm, CustomProfileChangeForm


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('email', 'is_staff', 'is_active',)
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('username', 'reports_interval', 'risk_level', 'email', 'password', 'domain1', 'domain2', 'domain3', 'domain4', 'domain5','has_started')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email',  'reports_interval', 'risk_level', 'password1', 'password2','has_started', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('username', 'reports_interval', 'risk_level', 'email', 'domain1',
                     'domain2', 'domain3', 'domain4', 'domain5','has_started')
    ordering = ('email', 'username', 'reports_interval', 'risk_level',  'domain1', 'domain2',
                'domain3', 'domain4', 'domain5','has_started')


# Register your models here.
admin.site.register(CustomUser, CustomUserAdmin)
