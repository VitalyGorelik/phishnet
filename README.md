# Phishnet Project #

This project objective is to find all of our customer's Phishing websites using Phishnet's SaaS solution.
The idea for the project came from the following article about a 97.67% detection hit approach to the phishing problem:
[Link to the Article](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3716033) 

### Disclaimer ###
* The website is hosted on AWS and is currently down in order to avoid high server costs.
* The project will only be ran on-demand.

### What do I need to do to run a phishing scan? ###
* Simply register to our website, and enter the domains you wish to protect.
* Then go to your Reports page and click on the 'Scan' button

### How do we find phishing websites? ###
1. We retrieve the user's registered domains from the DB.
   
2. For each domain we launch a process.
   
3. Each process creates a thread that generates permutation for that specific domain, by using well known phishing techniques such as replacing an 'i' with '1' and switching between character's positions.

4. We filter the suspects from step 3 to only the websites that their DNS is responding. (they are up)

5. We run an automation to fetch us a screenshot and the whole website's DOM both for the original site and the suspects from step 4.

6. We do a similarity comparison by the URL, screenshot, and the DOM elements to determine a risk score for each domain.

7. When done we are saving the suspects and their scores to our DB and trigger an email to the user that the report has finished using our SMTP emailing mechanism.

### Architecture ###
* We created an ec2 instance running Ubuntu 20.04 TLS
* We used Docker and Docker-Compose to build and run our containers
* The project consists of 4 containers:
    * Frontend and Backend - Python and Django
    * DB - Postgres
    * Reverse Proxy - Nginx 
    * Microservice - Algorithm manager

    

